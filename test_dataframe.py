#! /usr/bin/python



#  File:   test_dataframe.py.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on Apr 28, 2015, 2:26:49 PM


'''
    The HR predictive model: the model of the best customer.
    
    Part 2.  Data transformation example 
    

'''


__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 28, 2015 2:26:49 PM$"



# System modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd # pandas.Dataframe is being used for data processing

# a plotter module
import seaborn as  sns # seaborn to make a nice plots of the data
from VariableAnalysis import VariableAnalysis

# Transformers
import Normalizer # my own transformer
from sklearn import preprocessing # a set of transformers from sklearn


# Imputer
import DataFrameImputer # my own imputer

# Interpolators of the numerical data. Useful for simulations
from interpolate_histogram import histogram_interpolate

# Dataframe
from Dataframe import Dataframe




if __name__ == "__main__":
     
     
    prefix='HR_predictive_test_'
    
    features = [
        
        ('single_price', None), # our target
        ('p_status',Normalizer.NormalizerWrapper(type='normal')), # a label
        ('Kunde_comp',Normalizer.NormalizerWrapper(type='normal')), # a label
        ('source',preprocessing.LabelEncoder()), # a label
        ('s_anrede',preprocessing.MinMaxScaler()), # a categorical numerical value
        ('stars',Normalizer.NormalizerWrapper(type='normal')), # numerical value
        ('reise_dauer',Normalizer.NormalizerWrapper(type='normal')), # a numerical value with a large scale
        ('no_of_persons',Normalizer.NormalizerWrapper(type='normal')), # a numerical value
        ('Buch_anzahl',Normalizer.NormalizerWrapper(type='normal')), # !!!
        ('age',Normalizer.NormalizerWrapper(type='normal')), # !!!   
    

    
 
    
    ]
    
    # read csv file and make summary
    df=Dataframe("Test_adressen_150410.csv",features)
    df.summary()
     # fill missed data
    df.dataset = DataFrameImputer.DataFrameImputer().fit(df.dataset).transform(df.dataset)
    
    # Test 6: Transformation of the dataset
    df.transform_dataframe()
    df.summary(transformed=True)
    
    
    # Test transformation
    data = df.dataset.loc[0:4,['p_status','Buch_anzahl','age']]
    print 'data original=',data.head(5)
    df.transform_data(data)
    print 'data transformed=',data.head(5)
    

    
    
    # Test 7: plot original and transformed data
    sns.set_context("poster")
    
    # Example: p_status
    var='p_status'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricaltext')
    pl.bar([ 2*i  for i,j in enumerate(x_grid)], pdf, align='center',color='b',label=var)
    pl.xticks([ 2*i  for i,j in enumerate(x_grid)],x_grid,rotation=60)
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
    
    
    # Example: s_anrede
    var='s_anrede'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricaltext')
    pl.bar([ 2*i  for i,j in enumerate(x_grid)], pdf, align='center',color='b',label=var)
    pl.xticks([ 2*i  for i,j in enumerate(x_grid)],x_grid,rotation=60)
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
    
    # Example: age
    var='age'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricalnum')
    pl.bar(np.array(x_grid),pdf,color='b',width=1,label=var)    
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
    
     # Example: no_of_persons
    var='no_of_persons'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricalnum')
    pl.bar(np.array(x_grid),pdf,color='b',width=1,label=var)    
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
      # Example: Buch_anzahl
    var='Buch_anzahl'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricalnum')
    pl.bar(np.array(x_grid),pdf,color='b',width=1,label=var)    
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
    
    
    
      # Example: stars
    var='stars'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricalnum')
    pl.bar(np.array(x_grid),pdf,color='b',width=1,label=var)    
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
     
      # Example: Kunde_comp
    var='Kunde_comp'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricalnum')
    pl.bar(np.array(x_grid),pdf,color='b',width=1,label=var)    
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
    
    
     # Example: source
    var='source'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricaltext')
    pl.bar([ 2*i  for i,j in enumerate(x_grid)], pdf, align='center',color='b',label=var)
    pl.xticks([ 2*i  for i,j in enumerate(x_grid)],x_grid,rotation=60)
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
    
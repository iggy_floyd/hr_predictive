#! /usr/bin/python



#  File:   TransformWrapper.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on May 3, 2015, 12:20:56 PM




__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$May 3, 2015 12:20:56 PM$"



import sys

sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9



import matplotlib 
import numpy as np
import matplotlib.pyplot as pl

# a plotter module
import seaborn as  sns # seaborn to make a nice plots of the data
from VariableAnalysis import VariableAnalysis

# Transformers
import Normalizer # my own transformer
from sklearn import preprocessing # a set of transformers from sklearn

# Imputer
import DataFrameImputer # my own imputer

# Dataframe
from Dataframe import Dataframe


# validation and splitting for  train/test samples
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.cross_validation import cross_val_score, KFold
from sklearn.cross_validation import train_test_split

# Linear regressor
from sklearn.linear_model import LinearRegression # OLS
from sklearn.linear_model import Ridge # OLS with shrinkage
from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import LogisticRegression

# Extension of the Linear regressor by PolynomialFeatures
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline

# ignore DeprecateWarnings by sklearn
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

from sklearn.pipeline import Pipeline,FeatureUnion


prefix='HR_predictive_pipe_transform_test_'


class TransformWrapperLambda(object):
    def __init__(self,func) :
        self.func=func
    
    def fit(self,X,y=None):
        return self

    def fit_transform(self,X,y=None,**kwargs):
        return map(lambda x: self.func(x), X)

    def transform(self,X):
        return map(lambda x: self.func(x), X)
    
class TransformWrapperTransformer(object):
    def __init__(self,obj) :
        self.obj=obj
    
    def fit(self,X,y=None):
        self.obj.fit(X)
        return self

    def fit_transform(self,X,y=None,**kwargs):
        self.obj.fit(X)
        return map(lambda x: self.obj.transform(x), X)

    def transform(self,X):        
        return map(lambda x: self.obj.transform(x), X)








if __name__ == "__main__":
    
    
    
    pipe=Pipeline([
            ('lb',TransformWrapperTransformer(preprocessing.LabelEncoder())),
            ('float',TransformWrapperLambda(lambda x: float(x))),
            ('std',preprocessing.StandardScaler())
        ])

    features = [        
    
        ('stars',pipe), # numerical value
        
    ]
        
    # read csv file and make summary
    df=Dataframe("Test_adressen_150410.csv",features)

    # fill missed data
    df.dataset = DataFrameImputer.DataFrameImputer().fit(df.dataset).transform(df.dataset)

    #Transformation of the dataset
    df.transform_dataframe() 
    
    
    # Example: stars
    var='stars'
    
    
    # A transformed variable
    va=VariableAnalysis(var,df.datasettransformed,'float')
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='adaptive')
    pl.plot(x_grid,pdf,label=var,color='b')
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'.png')
    pl.show()
    
    # An original variable
    va=VariableAnalysis(var,df.dataset)   
    (x_grid,pdf,vals,peaksind)=va.likelihood(None,None,type='categoricaltext')
    pl.bar([ 2*i  for i,j in enumerate(x_grid)], pdf, align='center',color='b',label=var)
    pl.xticks([ 2*i  for i,j in enumerate(x_grid)],x_grid,rotation=60)
    pl.title(var)          
    pl.legend(loc='upper right')
    pl.savefig('plots/'+prefix+var+'_original.png')
    pl.show()
        
        

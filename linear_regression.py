#! /usr/bin/python



#  File:   linear_regression.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on Apr 28, 2015, 11:27:26 PM


'''
 The HR predictive model: the model of the best customer.
    
    Part 3.  Linear reggressors
    
'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 28, 2015 11:27:26 PM$"


# System modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd # pandas.Dataframe is being used for data processing

# a plotter module
import seaborn as  sns # seaborn to make a nice plots of the data
from VariableAnalysis import VariableAnalysis

# Transformers
import Normalizer # my own transformer
from sklearn import preprocessing # a set of transformers from sklearn


# Imputer
import DataFrameImputer # my own imputer

# Interpolators of the numerical data. Useful for simulations
from interpolate_histogram import histogram_interpolate

# Dataframe
from Dataframe import Dataframe

# validation and train/test samples splitting
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.cross_validation import cross_val_score, KFold
from sklearn.cross_validation import train_test_split

# Linear regressor
from sklearn.linear_model import LinearRegression # OLS
from sklearn.linear_model import Ridge # OLS with shrinkage
from sklearn.linear_model import Lasso, LassoCV, LassoLarsCV, LassoLarsIC
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import RANSACRegressor
from sklearn.linear_model import Perceptron


from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import SGDRegressor

# Extension of the Linear regressor by PolynomialFeatures
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


# ignore DeprecateWarnings by sklearn
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

 
def plot_best_features(best,X,y,features): 
    for b in best:
        
        pl.plot(X[:, b], y, 'o')
        pl.title("feature %s" % features[b] )
        pl.xlabel("X")
        pl.ylabel("Y")
        pl.show()


def validation_prediction(dataframe,estimator,index,train_cols,binarizer=None):
    
    data = dataframe.dataset.loc[[index],['single_price']+train_cols]        
    price_true=data['single_price'].as_matrix()[0]
    print "originaly, we have\n", data
    
    
    dataframe.transform_data(data)    
    print "after transformation, we have\n", data
    group_true = data['single_price'].as_matrix()[0]
    
    
    
    X = data[train_cols].as_matrix()
    
    if (binarizer): 
        X=binarizer.transform(X)
        #print "OneHotEncoder encoding: ",X
    
    pred=estimator.predict(X)[0]-1
    print "predicted price group", pred
    print "price group true", group_true
    
    
    if (pred >1 ):
        
        prices=np.array([1000,500,300,150,50,10])[-(pred+1):-(pred-1)]
        #prices=np.array([1000,500,300,150,100,50,10])[-(pred+1):-(pred-1)]
        #prices=np.array([1000,500,300,200,180,150,120,110,100,80,70,50,40,30,20,10])[-(pred+1):-(pred-1)]

    else:    
        prices=np.array([1000,500,300,150,50,10])[-(pred+1):]
        #prices=np.array([1000,500,300,150,100,50,10])[-(pred+1):]
        #prices=np.array([1000,500,300,200,180,150,120,110,100,80,70,50,40,30,20,10])[-(pred+1):]
    
    print "predicted price is in the range",prices
    print "price_true is ",price_true
    
    
    return 

if __name__ == "__main__":
    
    
    prefix='HR_predictive_test_'
    
    features = [
        
        #('single_price', None), # our target
        ('single_price', lambda x:  abs(x)), # our target
       # ('single_price', preprocessing.MinMaxScaler()), # our target
        ('p_status',Normalizer.NormalizerWrapper(type='normal')), # a label
        ('Kunde_comp',Normalizer.NormalizerWrapper(type='normal')), # a label
        ('s_anrede',preprocessing.MinMaxScaler()), # a categorical numerical value
        ('stars',Normalizer.NormalizerWrapper(type='normal')), # numerical value
        ('reise_dauer',Normalizer.NormalizerWrapper(type='normal')), # a numerical value with a large scale
        ('no_of_persons',Normalizer.NormalizerWrapper(type='normal')), # a numerical value
        ('Buch_anzahl',Normalizer.NormalizerWrapper(type='normal')), # !!!
        ('age',Normalizer.NormalizerWrapper(type='normal')), # !!! 
     
    
    ]
    
    # grouping price
    def single_price(x):
        
        x=int(abs(x))
        
        return len(np.where(np.array([ x/i for i in [1000,500,300,150,50,10]]) != 0)[0])
        #return len(np.where(np.array([ x/i for i in [1000,500,300,150,100,50,10]]) != 0)[0])
        #return len(np.where(np.array([ x/i for i in [1000,500,300,200,180,150,120,110,100,80,70,50,40,30,20,10]]) != 0)[0])
    
    # grouping reise_dauer
    def reise_dauer(x):
        x=int(abs(x))
        return len(np.where(np.array([ x/i for i in [360,180,90,30,15,10,7,2]]) != 0)[0])
    
     # grouping no_of_persons
    def no_of_persons(x):
        x=int(abs(x))
        return len(np.where(np.array([ x/i for i in [100,50,20,10,4,2]]) != 0)[0])
    
     # grouping age
    def age(x): 
        x=int(abs(x))        
        return len(np.where(np.array([ x/i for i in [80,60,50,40,30,20,18,16,10,5]]) != 0)[0])
    
    features = [
        
        #('single_price', None), # our target
        ('single_price', lambda x: single_price(x)), # our target       
        ('p_status',preprocessing.LabelEncoder()), # a label
        ('Kunde_comp',preprocessing.LabelEncoder()), # a label
        ('s_anrede',preprocessing.LabelEncoder()), # a categorical numerical value
        ('stars',preprocessing.LabelEncoder()), # numerical value
        ('reise_dauer',lambda x: reise_dauer(x)), # a numerical value with a large scale
        ('no_of_persons',lambda x: no_of_persons(x)), # a numerical value
        ('Buch_anzahl',lambda x: int(abs(x))), # !!!
        ('age',lambda x: age(x)), # !!! 
     
    
    ]
     
    
    
    # read csv file and make summary
    df=Dataframe("Test_adressen_150410.csv",features)
    
     # fill missed data
    df.dataset = DataFrameImputer.DataFrameImputer().fit(df.dataset).transform(df.dataset)
    
    # check some zero and negative values of the 'single_price'
    #print df.dataset.loc[df.dataset[df.dataset['single_price']<=0].index,['single_price']].head(100)
    #print df.dataset.loc[df.dataset[df.dataset['single_price']<0].index,['single_price']]
    

    #Transformation of the dataset
    df.transform_dataframe() 
    
    # target
    y = df.datasettransformed['single_price'].as_matrix()
    # the whole transformed dataset
    train_cols=[  feature[0] for feature in features if feature[0] != 'single_price']
    X = df.datasettransformed[train_cols].as_matrix()
    
    
    
    from sklearn.preprocessing import OneHotEncoder
    enc =None
    enc=OneHotEncoder()    
    X=enc.fit_transform(X)
    
    
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.6)
    
    #X /= np.sqrt(np.sum(X ** 2, axis=0))

    
    # Test 11:                                                                                                                                                                              
    # find best 4 features and train on them
    #selector = SelectKBest(f_regression, k=4).fit(X, y)
    #best_features = np.where(selector.get_support())[0]
    #print(best_features)
   
    #plot_best_features(best_features,X,y,train_cols)
   
    
   
    #Xt = X[:,best_features]
    Xt=X_train
    y=y_train
    #clf = LinearRegression().fit(Xt, y)
    clf = LogisticRegression().fit(Xt, y)
    print("Score is ", clf.score(Xt, y))
    print("Score is ", clf.score(X_test, y_test))
    
    
    clf = LogisticRegression(solver='lbfgs',multi_class='multinomial').fit(Xt, y)
    print("Score is ", clf.score(Xt, y))
    print("Score is ", clf.score(X_test, y_test))
        
    
    validation_prediction(df,clf,2,train_cols,binarizer=enc)
    validation_prediction(df,clf,3,train_cols,binarizer=enc)
    validation_prediction(df,clf,4,train_cols,binarizer=enc)
    validation_prediction(df,clf,5,train_cols,binarizer=enc)
    
    
    
    
    
    
    print cross_val_score(clf, X_test, y_test, cv=5)
    print cross_val_score(clf, X_test, y_test, cv=5).mean()
    
    
    
    

    Xt = X_train
    alpha= 1e-5
    for degree in [0, 1, 3, 4,5,6,7]:
        #est = make_pipeline(PolynomialFeatures(degree), LinearRegression())
        #est = make_pipeline(PolynomialFeatures(degree),SGDRegressor(    ))
       # est = make_pipeline(PolynomialFeatures(degree), Ridge(alpha=alpha))
        #est = make_pipeline(PolynomialFeatures(degree), Lasso(alpha=alpha))
        #est = make_pipeline(PolynomialFeatures(degree), LassoLarsIC(criterion='bic'))
        #est = make_pipeline(PolynomialFeatures(degree), LassoCV(cv=20))
        est = make_pipeline(PolynomialFeatures(degree), BayesianRidge())
        #est=LogisticRegression(C=100, penalty='l2', tol=0.01)
        #est=make_pipeline(PolynomialFeatures(degree),RANSACRegressor(random_state=42))
        #est=make_pipeline(PolynomialFeatures(degree,interaction_only=True),TheilSenRegressor(random_state=42))
        #est=make_pipeline(PolynomialFeatures(degree),LogisticRegression())        

 

        est.fit(X_train, y_train)
        print("Score is at %d degree"%degree, est.score(X_test, y_test))




 

'''
	This is a PDE Class designed to make Probability Density Estimator used in Projective Likelihood MA
'''

import pandas as pd
import numpy as np
import copy 
from sklearn.neighbors import KernelDensity
import matplotlib.pyplot as pl


class PDE(object):
 '''
	This is a PDE Class designed to make Probability Density Estimator used in Projective Likelihood MA
 '''

 def __init__(self,name,dataframe,typepde,type=None):

        self.name = name
        self.dataframe = dataframe        
        if (type is not None and not self.dataframe[name].isnull().any()):  #.astype(type)
            self.dataframe[name] = pd.DataFrame(self.dataframe[name],dtype=type)
        self.type = type
        # possible types: 'adaptive', 'small', 'big', 'standard', 'boolean', 'categoricalnum', 'categoricaltext'  
        self.typepde = typepde
        self.X_grid = None
        self.pdf = None
        self.vals = None
	pass


 # taken from  https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
 def kde(self,x, x_grid, bandwidth=0.5, **kwargs):

    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


 # taken from http://toyoizumilab.brain.riken.jp/hideaki/res/code/sskernel.py
 def kde_adaptive(self, x, x_grid):
    import sskernel
    ret = sskernel.sskernel(x,tin=x_grid, bootstrap=True)
    return ret['y']


 def makePDE(self,target,key,nsteps=100,X_grid=None):
    ''' possible types: 'adaptive', 'small', 'big', 'standard', 'boolean', 'categoricalnum', 'categoricaltext' '''    

    # extract values for kde creation
    vals =self.dataframe.loc[self.dataframe[self.dataframe[target] == key].index,self.name].as_matrix()  
    fullvals = self.dataframe.loc[:,self.name].as_matrix()  if ((self.typepde in 'categoricaltext') or (self.typepde  in 'categoricalnum')) else None
    self.vals = vals.tolist()
    kdes={
             'adaptive': None,
             'small': None,
             'big': None,
             'standard': None
         }


    if self.typepde  in kdes:
        if X_grid is None:
            minx=min(vals)
            maxx=max(vals)
            if (minx == maxx): minx = float(minx)/nsteps
            X_grid= np.linspace(float(minx),float(maxx), nsteps)        

        try:
            kdes = {
                'adaptive': self.kde_adaptive(vals,X_grid),
                'small':    self.kde(vals,X_grid, bandwidth=0.05),  # a small bandwith
                'big':      self.kde(vals,X_grid, bandwidth=1.5),   #  a big bandwith,to decrease fluctuations
                'standard': self.kde(vals,X_grid, bandwidth=0.5)  # a standard  bandwith
            }               
            pdf = kdes[self.typepde] 
            self.X_grid = X_grid.tolist()
            self.pdf=self.normalize(min(vals),max(vals),self.X_grid,pdf)
 
        
            
        except: 
            print "something wrong with kde"           
            return None
    elif self.typepde  in 'boolean':
        
        f1 = lambda x: x>0
        f2 = lambda x: x==0
       
        self.X_grid = [0,1]
        self.pdf = {0:float(len(filter(f2,vals.tolist())))/len(vals.tolist()),1:float(len(filter(f1,vals.tolist())))/len(vals.tolist())}
        
        

    elif ((self.typepde  in 'categoricaltext') or (self.typepde  in 'categoricalnum')):
        
        D = {}
        for i in vals.tolist(): 
            if i not in D: D[i] = 1
            else: D[i] += 1

        Dfullvals ={}
        for i in fullvals.tolist(): 
            if i not in Dfullvals: Dfullvals[i] = 1
            else: Dfullvals[i] += 1


        #import operator
        #if (self.typepde  in 'categoricalnum'):
        #    key = operator.itemgetter(0)
        #else:
        #    key = operator.itemgetter(1)
        #Dfullvals=sorted([(key,float(val)/len(vals.tolist())) for key,val in Dfullvals.items() ],key,reverse=False)
        #
        #self.X_grid = map(lambda x: x[0],Dfullvals)
        #self.pdf = map(lambda x: float(D[x[0]])/len(vals.tolist()) if x[0] in D else 0,Dfullvals)
        self.X_grid = None
        self.pdf = dict((key,float(D[key])/len(vals.tolist()) if key in D else 0) for key in Dfullvals.keys())
    return self

 def getPDE(self,x):

    
    kdes={
             'adaptive': None,
             'small': None,
             'big': None,
             'standard': None
         }
    if self.typepde  in kdes:
        if x > max(self.X_grid) or x < min(self.X_grid): 
        #    raise ValueError("Something wrong with %f which is %f > %f or %f < %f"%(x,x,max(self.X_grid),x,min(self.X_grid))) 
             return 0.
        return self.likelihood_at_point(x,self.X_grid,self.pdf)
    elif self.typepde  in 'boolean':
       if x != 0 and x != 1:
        #raise ValueError("Something wrong with %f: it should be binary"%x) 
        return 0.
       return self.pdf[x]
    elif ((self.typepde  in 'categoricaltext') or (self.typepde  in 'categoricalnum')):
        if not(x in self.pdf):
            #raise ValueError("Something wrong with %s" %(str(x)))
            return 0.
        return self.pdf[x]

 def takeClosest(self,myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    from bisect import bisect_left
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before  

 def likelihood_at_point(self,x,x_grid,pdf):
    x=self.takeClosest(x_grid,x)
    return pdf[x_grid.index(x)]
    
 #### Numerical integration (http://rosettacode.org/wiki/Numerical_integration)
 @staticmethod
 def simpson(f,x,h):
  return (f(x) + 4*f(x + h/2) + f(x+h))/6.0
 
 @staticmethod
 def left_rect(f,x,h):
  return f(x)
 @staticmethod
 def integrate( f, a, b, steps, meth):
   import __builtin__
   h = float(b-a)/steps
   ival = h * __builtin__.sum([meth(f, a+i*h, h) for i in range(steps)])
   return ival  

 @staticmethod
 def integrate_scipy(f, a, b):
   from scipy import integrate
   return integrate.romberg(f,a,b)


 def normalize(self,minx,maxx,x_grid,pdf):
   f = lambda x: self.likelihood_at_point(x,x_grid,pdf)
   intgrl= PDE.integrate(f,minx,maxx,len(x_grid),PDE.simpson)
   return map(lambda x: float(x)/intgrl,pdf) if intgrl !=0 else pdf 

if __name__ == '__main__':

 # Set-up some parameters and loading dataframe
 process_name='fps_fraud_classification'
 print "reading from ",process_name+'_transformed.csv',"...."
 dataframe = pd.read_csv(process_name+'_transformed.csv')

 
 # Test 1: get PDE for some continues distributions
 # pde 1:   'airport_dist'
 X='airport_dist'
 pde_ok=PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')
 pde_notok=PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')
 minx=  min(pde_ok.X_grid)
 maxx=  max(pde_ok.X_grid)
 print "PDE of 'OK' in %f is %f"%(float(minx+maxx)/2.,pde_ok.getPDE(float(minx+maxx)/2.))
 print "PDE of 'NOT_OK' in %f is %f"%(float(minx+maxx)/2.,pde_notok.getPDE(float(minx+maxx)/2.))


 # Test 2: create Projective 1-dim Likelihood
 
 def projective_likelihood (x,pde_ok,pde_notok):
    
    num = pde_ok.getPDE(x)
    denom = pde_ok.getPDE(x) + pde_notok.getPDE(x)
    if (denom != 0): return num/denom
    else: return 0.


 X='airport_dist'
 pde_ok=PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_OK')
 pde_notok=PDE(X,dataframe,typepde='adaptive',type='float').makePDE('status','CHECKED_NOT_OK')
 
 y_ok=map(lambda x: projective_likelihood(x,pde_ok,pde_notok),pde_ok.vals) 
 y_notok=map(lambda x: projective_likelihood(x,pde_ok,pde_notok),pde_notok.vals)
 
 nx, xbins, ptchs = pl.hist(y_ok, bins=50,normed=True)
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
 width = xbins[1] - xbins[0] # Width of each bin.
 x_ok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_ok = np.ravel(zip(nx_frac,nx_frac))
 
 nx, xbins, ptchs = pl.hist(y_notok, bins=50,normed=True)
 pl.clf() 
 nx_frac = nx/float(len(nx)) # Each bin divided by total number of objects.
 width = xbins[1] - xbins[0] # Width of each bin.
 x_notok = np.ravel(zip(xbins[:-1], xbins[:-1]+width))
 y_notok = np.ravel(zip(nx_frac,nx_frac))
 

 pl.plot(x_ok,y_ok,linestyle="dashed",label="OK")
 pl.plot(x_notok,y_notok,label="NOT_OK")
 pl.title('Projective likelihood estimator')          
 pl.legend(loc='upper right')
 pl.show()
 pl.show()
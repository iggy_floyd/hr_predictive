'''
 performs normalization of label distribution

'''
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as plt
import operator
from collections import OrderedDict
from scipy.stats import norm
import math
from math     import *
import pylab as pl


class  NormalizerWrapper(object):
    ''' a wrapper to add fit_transform() method to Normalizer'''
   

    def __init__(self,type = 'normal'):
        self.type = type
        return 

    def fit_transform(self,data):     
        self.normalizer = Normalizer(data).fit_normal() if  self.type in 'normal' else  (   
                    Normalizer(data).fit_decrease() if self.type in 'decrease' else  ( 
                        Normalizer(data).fit_median() if self.type in 'median' else None
                        )                        
                )
        if (type(data) == type([])):
            return map(lambda x: self.normalizer.transform(x),data)
        if (type(data) == type(np.array([]))):
            return map(lambda x: self.normalizer.transform(x),data.tolist())
    
        return data

    def transform(self,x):
        ''' 'x' might be a string or [string] '''
        return [self.normalizer.transform(x[0])] if type(x)==type([]) else self.normalizer.transform(x) 

    def inverse_transform(self,x):
        ''' 'x' might be a string or [string] '''
        return [self.normalizer.inverse_transform(x[0])] if type(x)==type([]) else self.normalizer.inverse_transform(x) 



class Normalizer(object):

 def __init__ (self,labels, removeitems=[]):

  if labels is not None:
   self.labels =  labels
   for item in removeitems:
    self.labels =  map(lambda x: x.replace(item,''),self.labels)   
   self.dictionary = Normalizer.text2dictionary(self.labels)
  else:
   self.labels = None
   self.dictionary = None
  self.transformed = None
  return


 # taken from http://www.johndcook.com/blog/python_phi_inverse/
 # there is an another implementation: http://johnkerl.org/python/normal_m.py.txt

 
 @staticmethod
 def normal_CDF_inverse(p):

    def _rational_approximation(t):
 
        # Abramowitz and Stegun formula 26.2.23.
        # The absolute value of the error should be less than 4.5 e-4.
        c = [2.515517, 0.802853, 0.010328]
        d = [1.432788, 0.189269, 0.001308]
        numerator = (c[2]*t + c[1])*t + c[0]
        denominator = ((d[2]*t + d[1])*t + d[0])*t + 1.0
        return t - numerator / denominator

 
    
    p = 1e-10 if p<0.0 else p
    p = 1. if p>1.0 else p
    #assert p > 0.0 and p < 1
    

    # See article above for explanation of this section.
    if p < 0.5:
        # F^-1(p) = - G^-1(p)
        return -_rational_approximation( math.sqrt(-2.0*math.log(p)) )
    else:
        # F^-1(p) = G^-1(1-p)
        return _rational_approximation( math.sqrt(-2.0*math.log(1.0-p)) )
 

 #johnkerl.org/python/normal_m.py.txt
 @staticmethod
 def normalcdf(x):
	return 0.5 * (1.0 + erf(x/sqrt(2)))

 def fit_normal(self):

  #get ratios
  _sum = sum([x[1] for x in self.dictionary])
  _ratios = [(x[0],float(x[1])/_sum) for x in  self.dictionary ]
  _max = float(self.dictionary[0][1])/_sum
  #_scale =  0.5/_max
  #_ratios = map(lambda x: (x[0],float(x[1])*_scale),  _ratios)

  def getProperties(ratios):
   m=ratios[0][1] 
   counter = 0
   for x in ratios:
    if x[1] < m:    return (m,x[1],counter)
    counter +=1

   return (m,m,counter)

  self.transformed = {}
  _orientation = True # True for left, False for right
  _left=0.0
  _right=0.0
  for x in _ratios:
    if (_left == _right):     
        self.transformed[x[0]] = 0.0    
        _left = Normalizer.normal_CDF_inverse(Normalizer.normalcdf(_left)-x[1]/2)
        _right = Normalizer.normal_CDF_inverse(Normalizer.normalcdf(_right)+x[1]/2)
    else:
        if _orientation:      
            self.transformed[x[0]] = _left/2.0
            _left = Normalizer.normal_CDF_inverse(Normalizer.normalcdf(_left)-x[1]/2)
            self.transformed[x[0]] += _left/2.0
            _orientation = False
        else:
            self.transformed[x[0]] = _right/2.0
            _right = Normalizer.normal_CDF_inverse(Normalizer.normalcdf(_right)+x[1]/2)
            self.transformed[x[0]] += _right/2.0
            _orientation = True

    #print x, self.transformed[x[0]]
  
  #print _ratios
  #print self.transformed
  #print getProperties(_ratios)
  return self
 

 def fit_decrease(self):
 
    start_position = -0.5
    self.transformed = {}
    for x in self.dictionary:
        start_position = start_position  + 1
        self.transformed[x[0]] = start_position 

    return self


 def fit_median(self):
    self.transformed = {}
    _orientation = True # True for left, False for right
    _left=0.0
    _right=0.0

    for x in self.dictionary:
        if (_left == _right):     
            self.transformed[x[0]] = 0.0    
            _left -=0.5 
            _right +=0.5 
        else:
            if _orientation:      
                self.transformed[x[0]] =_left 
                _left -=0.5             
                _orientation = False
            else:
                self.transformed[x[0]] = _right
                _right +=0.5
                _orientation = True

    
    return self

 def transform(self,label,update=False,updateType='normal'):
  ''' updateTypes: 'normal', 'decrease','median' '''

  if (self.transformed is None): return  -1e10
  if (not(label in self.transformed) and (not update)): 
      self.transformed['NaN'] = -1e10
      return  -1e10

  if (label in self.transformed): return self.transformed[label]

  return self.update([label],updateType).transformed[label]


 def inverse_transform(self,value):

    if (self.transformed is None): return  'NaN'

    # find the closest distance between two floats
    def dist(x,y):   
        return np.sqrt((x-y)**2)
  
    a = [(key,dist(value,val)) for key,val in self.transformed.items()]
    from operator import itemgetter
    return min(a,key=itemgetter(1))[0]



    #try:
    #    key=self.transformed.keys()[self.transformed.values().index(value)]
    #except ValueError: 
    #    return 'NaN'
    #return  key


 def update(self,labels,type='normal'):
 
  self.labels +=labels
  self.dictionary = Normalizer.text2dictionary(self.labels)
 
  if (type == 'normal'):    self.fit_normal()
  if (type == 'decrease'):    self.fit_decrease()
  if (type == 'median'):    self.fit_median()
  return self

 

 @staticmethod
 def text2dictionary(text):
 
  _dict = text
  #_dict = map(lambda x: x.replace('.',''),_dict)
  dct={}
  {w: 1 if w not in dct and not dct.update({w: 1})
                  else dct[w] + 1	
                  if not dct.update({w: dct[w] + 1}) else 1 for w in _dict}

	

  return sorted(dct.items(), key=operator.itemgetter(1),reverse=True)


 def plot(self,values=None):
  if self.dictionary is None: return
  pl.figure(figsize=(10, 10))  
  numsubplots = 2 if self.transformed else 1
  pl.subplot(numsubplots ,1,1)
  pl.bar(range(len(self.dictionary)), map(lambda x: x[1], self.dictionary), align='center')
  pl.xticks(range(len(self.dictionary)), map(lambda x: x[0], self.dictionary))
  pl.ylabel('Frequency')
  if self.transformed:   
   pl.subplot(numsubplots,1,2)
   xbins=[i for i in self.transformed.values()]  
   xbins=sorted(xbins)
   if (values is None): 
    pl.hist(map(lambda x: self.transform(x),self.labels),bins=xbins)
   else:
    pl.hist(map(lambda x: self.transform(x),values),bins=xbins)
   #pl.hist(map(lambda x: self.transform(x),self.labels))
   pl.xlabel('transformed')
   pl.ylabel('Frequency')

  plt.show()

  


if __name__ == "__main__":
 
 # test 1: get rate of words in the text
 dictionary = '''Certainly elsewhere my do allowance at. The address farther six hearted hundred towards husband. Are securing off occasion remember daughter replying. Held that feel his see own yet. Strangers ye to he sometimes propriety in. She right plate seven has. Bed who perceive judgment did marianne. 

Ought these are balls place mrs their times add she. Taken no great widow spoke of it small. Genius use except son esteem merely her limits. Sons park by do make on. It do oh cottage offered cottage in written. Especially of dissimilar up attachment themselves by interested boisterous. Linen mrs seems men table. Jennings dashwood to quitting marriage bachelor in. On as conviction in of appearance apartments boisterous. 

Dissuade ecstatic and properly saw entirely sir why laughter endeavor. In on my jointure horrible margaret suitable he followed speedily. Indeed vanity excuse or mr lovers of on. By offer scale an stuff. Blush be sorry no sight. Sang lose of hour then he left find. 

Sociable on as carriage my position weddings raillery consider. Peculiar trifling absolute and wandered vicinity property yet. The and collecting motionless difficulty son. His hearing staying ten colonel met. Sex drew six easy four dear cold deny. Moderate children at of outweigh it. Unsatiable it considered invitation he travelling insensible. Consulted admitting oh mr up as described acuteness propriety moonlight. 

Delightful unreserved impossible few estimating men favourable see entreaties. She propriety immediate was improving. He or entrance humoured likewise moderate. Much nor game son say feel. Fat make met can must form into gate. Me we offending prevailed discovery. 

Sudden looked elinor off gay estate nor silent. Son read such next see the rest two. Was use extent old entire sussex. Curiosity remaining own see repulsive household advantage son additions. Supposing exquisite daughters eagerness why repulsive for. Praise turned it lovers be warmly by. Little do it eldest former be if. 

Excited him now natural saw passage offices you minuter. At by asked being court hopes. Farther so friends am to detract. Forbade concern do private be. Offending residence but men engrossed shy. Pretend am earnest offered arrived company so on. Felicity informed yet had admitted strictly how you. 

Attachment apartments in delightful by motionless it no. And now she burst sir learn total. Hearing hearted shewing own ask. Solicitude uncommonly use her motionless not collecting age. The properly servants required mistaken outlived bed and. Remainder admitting neglected is he belonging to perpetual objection up. Has widen too you decay begin which asked equal any. 

It allowance prevailed enjoyment in it. Calling observe for who pressed raising his. Can connection instrument astonished unaffected his motionless preference. Announcing say boy precaution unaffected difficulty alteration him. Above be would at so going heard. Engaged at village at am equally proceed. Settle nay length almost ham direct extent. Agreement for listening remainder get attention law acuteness day. Now whatever surprise resolved elegance indulged own way outlived. 

Am finished rejoiced drawings so he elegance. Set lose dear upon had two its what seen. Held she sir how know what such whom. Esteem put uneasy set piqued son depend her others. Two dear held mrs feet view her old fine. Bore can led than how has rank. Discovery any extensive has commanded direction. Short at front which blind as. Ye as procuring unwilling principle by. '''
 
 normalizer = Normalizer(dictionary.split())
 print normalizer.dictionary

 # test 2: fit method
 normalizer = normalizer.fit_normal()

 # test 3: transform method
 print normalizer.transform('it')
 print normalizer.transform('asked')
 print normalizer.transform('Farther')

 for x in normalizer.dictionary:
  print x[0],normalizer.transform(x[0])

 # test 4: plot distributions
 normalizer.plot()
 

 # test 5: test decreasing fit
 normalizer.fit_decrease().plot()

 # test 6: test median fit
 normalizer.fit_median().plot()

 

#! /usr/bin/python



#  File:   Dataframe.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on Apr 27, 2015, 7:07:36 PM



'''

    The HR predictive model: the model of the best customer.
    Part 1. Dataframe of available data example 
    
    
    TODO- random points selection for plotting

'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 27, 2015 7:07:36 PM$"

# System modules
import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl
import pandas as pd # pandas.Dataframe is being used for data processing

# a plotter module
import seaborn as  sns # seaborn to make a nice plots of the data


# Transformers
import Normalizer # my own transformer
from sklearn import preprocessing # a set of transformers from sklearn


# Imputer
import DataFrameImputer # my own imputer

# Interpolators of the numerical data. Useful for simulations
from interpolate_histogram import histogram_interpolate



class Dataframe(object):
    ''' An example of the data processing for HR predictive mode'''
    
    
    
    def __init__(self,filename,features,keep_default_na = False):
        self.filename = filename # name of the file for the  csv data  
        self.features = features
        self.dataset = None
        if ( not(keep_default_na )): # we don't want to store NaN values in the dataframe
            #self.dataset = pd.read_csv(filename,na_values=[' ',''],keep_default_na = False,sep=",")
            self.dataset = pd.read_csv(filename,na_values=[' ',''],keep_default_na = True,sep=",",encoding='utf-8')    
        
        
        self.train_cols = None
        self.datasettransformed = None
        self.transformers={}
    
    def transform_dataframe(self,doFit=True):
        
        if (self.dataset is None ): raise ValueError('dataset is not initialized')
        self.datasettransformed = self.dataset.copy(deep=True)
        
        for feature in self.features:
         name_feature = feature[0]
         transformator = feature[1]      
     
         if transformator:
             if ('function' in str(type(transformator))):                      
                 self.datasettransformed[name_feature] = self.datasettransformed[name_feature].apply(lambda x: transformator(x))
                
                 self.transformers[name_feature] = transformator
                 #print "function",name_feature

             elif ('class' in str(type(transformator))) or ('instance' in str(type(transformator))):
                 
                 #print "class",name_feature
                 if (doFit): transformator.fit_transform(self.datasettransformed[name_feature].as_matrix()) 
                 self.datasettransformed[name_feature] = self.datasettransformed[name_feature].apply(lambda x: transformator.transform([x])[0])                                
                 self.transformers[name_feature] = transformator
        return self    
    
        
    def transform_data(self,data):
        if ((data is None) or (len(data) == 0)): raise ValueError('data is emtpy')
        
        for feature in data:
            #print feature
            #print self.transformers[feature]
            #print type(self.transformers[feature])
            if hasattr(self.transformers[feature],'transform'):        
                data[feature] = data[feature].apply(lambda x: self.transformers[feature].transform([x])[0])
            else: 
                data[feature] = data[feature].apply(lambda x: self.transformers[feature](x)) 
        return self    
        
    def summary(self,transformed=False):
        if (not (transformed) and (self.dataset is not None)):
          print self.dataset.describe()
        elif (transformed and (self.datasettransformed is not None)):
          print self.datasettransformed.describe()
            
        return self
    
    
    def plot_dataframe(self,prefix,what_to_plot=None): 
        sns.set_context("poster")
        pl.rc("figure", figsize=(16, 10))
        if (what_to_plot is None):
            fig=self.dataset.hist()
        elif (not(what_to_plot is None) and type(what_to_plot) == type([])):
            fig=self.dataset[what_to_plot].hist()
            
            
        pl.savefig('plots/'+prefix+'_whole_dataframe_pandas.png', dpi=150)     
        pl.show()
        return self
        #return fig
 
 
    def seaborn_plots_lm(self,dataframe=None,prefix='seaborn_lm_',what_to_plot=None,_args={}):
        ''' to make correlation plots and linear model analysis '''
    
        if (dataframe is None): 
            dataframe = self.dataset
        
        
        def _plot_data(**args):  
        
            sns.set_context("poster")
            #sns.set_context("talk", font_scale=1.25)
            #pl.rc("figure", figsize=(16, 10))
            #fig = pl.figure(figsize=(24,10))
            #axes = pl.gca()
            

            X=args['X']
            Y=args['Y']
            prefix=args['prefix']
            dataframe=args['dataframe']
   
            del args['X'],args['Y'],args['dataframe'],args['prefix']
        
        
            # if Nan values, do some treatment 
            if any(pd.isnull(dataframe[X])): 
                    dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[X]).transform(dataframe[X])
                    for x in dataframe2[X]: dataframe[x] = dataframe2[x]
            if any(pd.isnull(dataframe[Y])):  
                    dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[Y]).transform(dataframe[Y])
                    for x in dataframe2[Y]: dataframe[x] = dataframe2[x]

     
            # linear analysis 
            #sns.lmplot(X,Y,dataframe,ax=axes,**args)
            sns.lmplot(X,Y,dataframe,**args)
            pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
            pl.show()
            return 

        
        args={
            'dataframe':dataframe,
            'X':'airport_dist', 'Y':'product_price','prefix':prefix, 
            'palette':'Set1', 'fit_reg':True
            }
        
        args = dict(args.items() + _args.items())
        
        if (not(what_to_plot is None) and type(what_to_plot) == type([])):    
            for plot in what_to_plot:
                args['X']=plot[0]
                args['Y']=plot[1]
                _plot_data(**args)
            return self
        
        
        
            # some numerical data of HR
        what_to_plot = [    
                'reise_dauer',
                'no_of_persons',
                'stars',
                'Buch_anzahl',
                'age'
              
        ]  
 

        for i,name in enumerate(what_to_plot):
            for j in range(i,len(what_to_plot)):
              args['X']=name
              args['Y']=what_to_plot[j]
              _plot_data(**args)

        return self
 
    def seaborn_plots_joint(self,dataframe=None,prefix='seaborn_joint_',what_to_plot=None,_args={}):
            ''' to make correlation plots and linear model analysis '''
            
            if (dataframe is None): 
                dataframe = self.dataset
    
            def _plot_data(**args):  
                sns.set_context("poster")
                #sns.set_context("talk", font_scale=1.25)
                #pl.rc("figure", figsize=(16, 10))
               
                X=args['X']
                Y=args['Y']
                prefix=args['prefix']
                dataframe=args['dataframe']
                del args['X'],args['Y'],args['dataframe'],args['prefix']
        
                # if Nan values, do some treatment 
                if any(pd.isnull(dataframe[X])): 
                    dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[X]).transform(dataframe[X])
                    for x in dataframe2[X]: dataframe[x] = dataframe2[x]
                if any(pd.isnull(dataframe[Y])):  
                    dataframe2 = DataFrameImputer.DataFrameImputer().fit(dataframe[Y]).transform(dataframe[Y])
                    for x in dataframe2[Y]: dataframe[x] = dataframe2[x]

                sns.jointplot(X,Y,dataframe,**args)          
                pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
                pl.show()
                return 


            args={
                 'dataframe':dataframe,
                 'X':'airport_dist', 'Y':'product_price','prefix':prefix, 
                 'kind':'reg','color':'seagreen'
                }
                
            
            args = dict(args.items() + _args.items())
        
            if (not(what_to_plot is None) and type(what_to_plot) == type([])):    
                for plot in what_to_plot:
                 args['X']=plot[0]
                 args['Y']=plot[1]
                 _plot_data(**args)
                return self
                
            # some numerical data of HR
            what_to_plot = [    
                'reise_dauer',
                'no_of_persons',
                'stars',
                'Buch_anzahl',
                'age'
              
                ]

            for i,name in enumerate(what_to_plot):
                for j in range(i+1,len(what_to_plot)):
                    args['X']=name
                    args['Y']=what_to_plot[j]
                    _plot_data(**args)

            return self
        
    def seaborn_plots_correlation(self,dataframe=None,prefix='seaborn_corr_',what_to_plot=None,_args={}):
        ''' to make correlation plots and linear model analysis '''
        
         
        if (dataframe is None): 
                dataframe = self.dataset
    
    
        def _plot_data(**args):  
 
            #sns.set_context("poster")
            sns.set_context("talk", font_scale=1.25)
            pl.rc("figure", figsize=(16, 10))

      
            X=args['X']
            prefix=args['prefix']
            dataframe=args['dataframe']

            del args['X'],args['dataframe'],args['prefix']
            
            data =dataframe.loc[:,X]
              # correlation plot 

            sns.corrplot(data)
 
               
            pl.savefig('plots/'+prefix+'_correlation.png')
            pl.show()
            return 

        args={
            'dataframe':dataframe,
            'X':'airport_dist','prefix':prefix
            }
            
        args = dict(args.items() + _args.items())
        
        if (not(what_to_plot is None) and type(what_to_plot) == type([])):    
            args['X']=what_to_plot  
            _plot_data(**args)
            return self    
   
            # some numerical data of HR
        what_to_plot = [    
                'reise_dauer',
                'no_of_persons',
                'stars',
                'Buch_anzahl',
                'age',
                'single_price'
                
              
       ]
 
        args['X']=what_to_plot
          

            
        _plot_data(**args)

        return self
    
    def seaborn_plots_dist(self,dataframe=None,prefix='seaborn_dist_',what_to_plot=None,_args={}):
        ''' to make correlation plots and linear model analysis '''
    
        if (dataframe is None): 
            dataframe = self.dataset
            
            
        def _plot_data(**args):  


            sns.set_context("poster")
            #sns.set_context("talk", font_scale=1.25)
               
            f, axes = pl.subplots(1, 1, figsize=(24, 10), sharex=True)
            pl.rc("figure", figsize=(16, 10))
            b, g, r, p = sns.color_palette("muted", 4)
            sns.despine(left=True)

            X=args['X']
            prefix=args['prefix']
            dataframe=args['dataframe']
            del args['X'],args['dataframe'],args['prefix']
            
            data =dataframe.loc[:,  [X]]
    
            # if Nan values, do some treatment 
            if any(pd.isnull(data)):  
                dataframe = DataFrameImputer.DataFrameImputer().fit(data).transform(data)
                for x in data: data[x] = dataframe[x]
            

        # distributution plot 
            sns.distplot(data.as_matrix(),color=b, ax=axes,**args)
            #sns.distplot(data.as_matrix(),color=b, **args)
        
       
            pl.savefig('plots/'+prefix+str(X)+'.png')
            pl.show()
            return  

      

        args={
            'dataframe':dataframe,
            'X':'airport_dist','prefix':prefix,
            'axlabel':'airport_dist'
        }
        
        args = dict(args.items() + _args.items())
        
        if (what_to_plot is None) or type(what_to_plot) != type([]) :
            
            what_to_plot = [ 'reise_dauer','no_of_persons',
                'stars',
                'Buch_anzahl',
                'age'   ]
    
        for i,name in enumerate(what_to_plot):
        
            args['X']=name
            args['axlabel']=name
            
            _plot_data(**args)

        return self
    
    
    
    def seaborn_plots_factor_num(self,dataframe=None,prefix='seaborn_factor_',what_to_plot=None,_args={}):
        ''' to make correlation plots and linear model analysis '''
        
        
        if (dataframe is None): 
            dataframe = self.dataset
            
                  
    
        def _plot_data(**args):  
 
            sns.set_context("talk", font_scale=1.25)
            X=args['X']
            Y=args['Y']
            prefix=args['prefix']
            dataframe=args['dataframe']
            xlabel = args['X']
            
            del args['X'],args['Y'],args['dataframe'],args['prefix']

            g=sns.factorplot(X,Y,data=dataframe,**args)
            g.set_xticklabels(rotation=60)
  
            locs,labels = pl.xticks()
            pl.xticks(locs, map(lambda x: "%.2f" % x, locs))
            pl.xlabel(xlabel)
            pl.title(xlabel)
        
            pl.savefig('plots/'+prefix+str(X)+str(Y)+'.png')
            pl.show()
            return 


        args={
            'dataframe':dataframe,
            'X':'airport_dist', 'Y':'single_price','prefix':prefix, 
            
        }
        
        args = dict(args.items() + _args.items())
        
        
        if (what_to_plot is None) or type(what_to_plot) != type([]) :
            
             what_to_plot = [
                    'no_of_persons'
                ]
        
       
 

        for i,name in enumerate(what_to_plot):
            args['X']=name
            _plot_data(**args)

        return self
    
    
    
    def seaborn_plots_coeff(self,dataframe,prefix='seaborn_coeffplot_',what_to_plot=None,_args={}):
        ''' to make correlation plots and linear model analysis '''
    
        if (dataframe is None): 
            dataframe = self.dataset
            
    
        def _plot_data(**args):  
 
   
            sns.set_context("poster")
            X=args['X']
            prefix=args['prefix']
            label=args['label']
            dataframe=args['dataframe']
            group = args['group']
            del args['X'],args['dataframe'],args['prefix'],args['label'],  args['group']

            

            # linear analysis 
            pl.rc("figure", figsize=(16, 10))
            if (group):        sns.coefplot(X, dataframe,group,**args)
            else:    sns.coefplot(X, dataframe,**args)
            
            pl.title(str(X).split('~')[0])
            pl.savefig('plots/'+prefix+str(X).split('~')[0]+label+'.png')
            pl.show()

            return 


        args={
            'dataframe':dataframe,
            'X':'airport_dist', 
            'prefix':prefix, 
            'palette':'muted',
            'label':'label',
            
            'group':None,                      
            'ci':68,       
            'intercept':False
         }
    
        args = dict(args.items() + _args.items())
        
        if (what_to_plot is None) or type(what_to_plot) != type([]) : 
            what_to_plot = [
         
                ('single_price ~   reise_dauer','reise_dauer'),       
                ('single_price ~ no_of_persons + reise_dauer ','no_of_persons'),
                ('single_price ~ reise_dauer + stars','stars','stars'),
                ('single_price ~ Buch_anzahl*age','age'),
        
        
            ]
        
        for i,name in enumerate(what_to_plot):
            args['X']=name[0]
            args['label']=name[1]
            
            if (len(name)>2):  
                if (len(name[2])>0):
                    args['group']=name[2]
                    if 'ci' in args: del args['ci']
                else: 
                    args['group']=None
                    args['ci']=68
            if (len(name)>3):  
                #args['intercept']=name[3]
                args['intercept']=True
           
            try:
                _plot_data(**args)
            except: 
              pass
            
        return self
  
  
if __name__ == "__main__":
    
    prefix='HR_predictive_test_'
    
    # Test 1: read csv file and make summary
    df=Dataframe("Test_adressen_150410.csv",None)
    df.summary()
    
     # fill missed data
    df.dataset = DataFrameImputer.DataFrameImputer().fit(df.dataset).transform(df.dataset)
    df.summary()
    
    # print first 10 entrie  
    print df.dataset.head(10)
    print df.dataset.dtypes # print types of the data in the dataframe

    
    # Test 2: plot a few distributions: a native pandas' plotter
    what_to_plot = [    
                'reise_dauer',
                'no_of_persons',
                #'stars',
                #'Buch_anzahl',
                'age'
        ]  
    df.plot_dataframe(prefix,what_to_plot=what_to_plot)
    
    # Test 3: make a nice 2-dim plots: a seaborn tool
    # It takes some time!
    
    what_to_plot = [   
                    ('reise_dauer','no_of_persons'),
                    ('reise_dauer','age'),
                    ('reise_dauer','no_of_persons'),
                    ('single_price','age'),
                    ]
    args={
        #'hue':'stars', # combine plotson 'star' 
        'hue':'p_status',  # combine plotson 'p_status' 
        'size':8, 'aspect':2
    }                
    #df.seaborn_plots_lm(dataframe=None,prefix=prefix+'seaborn_lm_',what_to_plot=what_to_plot,_args=args)
    
    
    
    what_to_plot = [   
                    #('reise_dauer','no_of_persons'),
                    #('reise_dauer','age'),
                    #('reise_dauer','no_of_persons'),
                    ('single_price','age'),
                    ]
    args={
    'size':20, 'ratio':8
    }  
    #df.seaborn_plots_joint(dataframe=None,prefix=prefix+'seaborn_joint_',what_to_plot=what_to_plot,_args=args)
    
    what_to_plot = [    
                'reise_dauer',
                'no_of_persons',
                'stars',
                'Buch_anzahl',
                'age'
              
       ]
    #df.seaborn_plots_correlation(dataframe=None,prefix=prefix+'seaborn_correlation_',what_to_plot=what_to_plot,_args={})
    
    
     
    what_to_plot = [ 
                #'reise_dauer',
                #'no_of_persons',
                #'stars',
                'Buch_anzahl',
                'age',
                'single_price'
                ]
    #df.seaborn_plots_dist(dataframe=None,prefix=prefix+'seaborn_dist_',what_to_plot=what_to_plot,_args={})
   
   
    args={
        'hue':'s_anrede','size':10, 'aspect':2
        }
        
    what_to_plot = [
                    'no_of_persons',
                    'single_price',
                    'Buch_anzahl'
                ]
            
    #df.seaborn_plots_factor_num(dataframe=None,prefix=prefix+'seaborn_factor_',what_to_plot=what_to_plot,_args=args)
    
    
    what_to_plot = [
         
                ('single_price ~   reise_dauer','reise_dauer'),       
                ('single_price ~ no_of_persons + reise_dauer ','no_of_persons'),
                ('single_price ~ reise_dauer + stars','reise_dauer','reise_dauer'),
                ('single_price ~ reise_dauer + age + no_of_persons + Buch_anzahl','Buch_anzahl','Buch_anzahl'),
                
        
        
            ]
    
    df.seaborn_plots_coeff(dataframe=None,prefix=prefix+'seaborn_coeff_',what_to_plot=what_to_plot,_args={})
    
    
    # Test 4. Some control checks
    s_anrede_2 = df.dataset.loc[df.dataset[df.dataset['s_anrede'] == 2].index,['s_anrede']]
    print "Number of entries with s_anrede = 2 is %d"%len(s_anrede_2)
    
    
    
    # Test5. Simulation of data: Buch_anzahl,  reise_dauer and s_anrede
    sns.set_context("poster")
    Buch_anzahl_data=df.dataset['Buch_anzahl'].as_matrix()
    reise_dauer_data=df.dataset['reise_dauer'].as_matrix()
    
    counts_Buch_anzahl, bins_Buch_anzahl = np.histogram(Buch_anzahl_data, bins=20)
    counts_reise_dauer, bins_reise_dauer = np.histogram(reise_dauer_data, bins=20)
    
    hi_Buch_anzahl = histogram_interpolate(counts_Buch_anzahl,bins_Buch_anzahl).create_interpolation(kind='cubic')
    hi_Buch_anzahl.plot() # test plot
    
    hi_reise_dauer = histogram_interpolate(counts_reise_dauer,bins_reise_dauer).create_interpolation(kind='cubic')
    hi_reise_dauer.plot() # test plot
    
    
    #  generate a random value according to the histograms
    histo_Buch_anzahl = hi_Buch_anzahl.create_inverse_interpolation(kind='linear').generate_random(10000)
    pl.hist(histo_Buch_anzahl,bins=bins_Buch_anzahl)
    pl.show()
    

    histo_reise_dauer = hi_reise_dauer.create_inverse_interpolation(kind='linear').generate_random(10000)
    pl.hist(histo_reise_dauer,bins=bins_reise_dauer)
    pl.show()
    
    
    s_anrede_data=df.dataset['s_anrede'].as_matrix()
    counts_s_anrede, bins_s_anrede = np.histogram(s_anrede_data, bins=4)
    hi_s_anrede = histogram_interpolate(counts_s_anrede,bins_s_anrede).create_interpolation(kind='cubic')
    
    histo_s_anrede = hi_s_anrede.create_inverse_interpolation(kind='linear').generate_random(10000)
    pl.hist(histo_s_anrede,bins=bins_s_anrede)
    pl.show()
    